;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:

(setq doom-font (font-spec :family "Iosevka Fixed" :size 15 :weight 'semi-light)
      doom-variable-pitch-font (font-spec :family "Iosevka" :size 15))

;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'modus-operandi)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'visual)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/")

;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; custom keybindings
(map! :leader
      :desc "Maximize window"
      "m" #'doom/window-maximize-buffer)

;; make physical keyboard dvorak so input methods work
(after! quail
  (add-to-list 'quail-keyboard-layout-alist
               `("dvorak" . ,(concat "                              "
                                     "  1!2@3#4$5%6^7&8*9(0)[{]}`~  "
                                     "  '\",<.>pPyYfFgGcCrRlL/?=+    "
                                     "  aAoOeEuUiIdDhHtTnNsS-_\\|    "
                                     "  ;:qQjJkKxXbBmMwWvVzZ        "
                                     "                              ")))
  (quail-set-keyboard-layout "dvorak"))

;; make easymotion use dvorak home keys
(setq avy-keys '(?a ?o ?e ?u ?i ?d ?h ?t ?n ?s))

(setq avy-dispatch-alist
      '((?x . avy-action-kill-stay)
        (?X . avy-action-kill-move)
        (?r . avy-action-teleport)
        (?v . avy-action-mark)
        (?y . avy-action-copy)
        (?p . avy-action-yank)
        (?P . avy-action-yank-line)
        (?l . avy-action-ispell)
        (?z . avy-action-zap-to-char)))

;; Set ErgoArabic input method
(add-load-path! "lisp")

(register-input-method
 "ergoarabic" "ErgoArabic" 'quail-use-package
 "ر" "ErgoArabic layout input method"
 "ergoarabic")

(setq default-input-method 'ergoarabic)

;;;; spell checking
(after! ispell
  (setq ispell-program-name "hunspell")
  (setq ispell-dictionary "en_US,ar")
  (setq ispell-personal-dictionary "~/.hunspell_personal")
  (setenv "LANG" "en_US.UTF-8")
  (unless (file-exists-p ispell-personal-dictionary)
    (write-region "" nil ispell-personal-dictionary nil 0))
  (ispell-set-spellchecker-params)
  (ispell-hunspell-add-multi-dic "en_US,ar"))

;; completion
(after! orderless
  (setq orderless-matching-styles '(orderless-literal orderless-flex
                                    orderless-initialism orderless-regexp)))

;; evil mode
(setq +evil-want-o/O-to-continue-comments nil)

;; disable evil-snipe
(remove-hook 'doom-first-input-hook #'evil-snipe-mode)
